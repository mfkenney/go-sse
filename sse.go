// Package sse implements HTTP Server Sent Events. It supports the http.Handler
// interface to allow it to be used with http.ServeMux:
//
//    mux := http.NewServeMux()
//    b := NewBroker()
//    mux.Handle("/stream", b)
//
package sse

import (
	"fmt"
	"log"
	"net/http"
	"sync"
)

// Event represents a Server Sent Event data message.
type Event struct {
	// Event name
	Name string
	// Event data
	Data []byte
}

// String implements the Stringer interface.
func (ev Event) String() string {
	return fmt.Sprintf("event: %s\ndata: %s\n\n", ev.Name, ev.Data)
}

// Broker receives Events and forwards them to all registered clients
type Broker struct {
	// Channel to receive Events which will be forwarded
	// to all registered clients.
	notifier chan Event
	// New client connections
	newClient chan chan Event
	// Closed connections
	closeClient chan chan Event
	// Client registry
	registry map[chan Event]bool
	cond     *sync.Cond
}

// Notify sends a new Event to the Broker.
func (b *Broker) Notify(ev Event) {
	b.notifier <- ev
}

// WaitForClient returns when at least one client is connected to the Broker.
func (b *Broker) WaitForClient() {
	b.cond.L.Lock()
	b.cond.Wait()
	b.cond.L.Unlock()
}

func (b *Broker) listen() {
	for {
		select {
		case c := <-b.newClient:
			b.cond.L.Lock()
			b.registry[c] = true
			b.cond.Broadcast()
			b.cond.L.Unlock()
			log.Printf("Client registered [%d]", len(b.registry))
		case c := <-b.closeClient:
			close(c)
			delete(b.registry, c)
			log.Printf("Client removed [%d]", len(b.registry))
		case ev := <-b.notifier:
			for ch, _ := range b.registry {
				ch <- ev
			}
		}
	}
}

// ServeHttp implements the http.Handler interface.
func (b *Broker) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	// Flushing must be supported so we can stream the response.
	flusher, ok := rw.(http.Flusher)
	if !ok {
		http.Error(rw, "Streaming unsupported!", http.StatusInternalServerError)
		return
	}

	// Set the headers related to event streaming.
	rw.Header().Set("Content-Type", "text/event-stream")
	rw.Header().Set("Cache-Control", "no-cache")
	rw.Header().Set("Connection", "keep-alive")
	rw.Header().Set("Access-Control-Allow-Origin", "*")
	rw.Header().Set("X-Accel-Buffering", "no")

	// Register the new client and arrange to unregister at function exit
	ch := make(chan Event)
	b.newClient <- ch

	// Listen for connection close event and unregister the client
	// if that occurs.
	go func() {
		<-req.Context().Done()
		b.closeClient <- ch
	}()

	// Wait for Events on our client channel and forward them over the HTTP
	// link in SSE format.
	for ev := range ch {
		fmt.Fprintf(rw, "%s", ev)
		flusher.Flush()
	}
}

// NewBroker creates a new Broker and starts it listening for Events
func NewBroker() *Broker {
	b := &Broker{
		notifier:    make(chan Event),
		newClient:   make(chan chan Event),
		closeClient: make(chan chan Event),
		registry:    make(map[chan Event]bool),
	}
	m := sync.Mutex{}
	b.cond = sync.NewCond(&m)

	go b.listen()

	return b
}
