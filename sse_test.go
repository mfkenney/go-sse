package sse

import (
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
)

var testData = struct {
	Text string
	Val  int
}{Text: "Hello World", Val: 42}

func makeEvent() Event {
	buf, _ := json.Marshal(testData)
	return Event{Name: "test", Data: buf}
}

func TestStream(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	req, err := http.NewRequestWithContext(ctx, "GET", "/stream", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	var wg sync.WaitGroup
	b := NewBroker()
	wg.Add(1)
	go func() {
		defer wg.Done()
		b.ServeHTTP(rr, req)
	}()
	b.WaitForClient()
	b.Notify(makeEvent())
	cancel()
	wg.Wait()

	if rr.Code != http.StatusOK {
		t.Errorf("Bad status; expected %d, got %d", http.StatusOK, rr.Code)
	}

	expected := makeEvent().String()
	result := rr.Body.String()
	if result != expected {
		t.Errorf("Bad result; expected %q, got %q", expected, result)
	}
}
